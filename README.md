# Crossbooking

**Crossbooking** - сервис обмена книгами через зоны буккроссинга

[Техническое задание](./doc/task.md)


Реализовано как клиент-серверное приложени.

Сервер хранит данные в Postgresql и предоставляет grpc сервисы для взаимодействия.

Клиент через эти сервисы запрашивает и отправляет данные, локально предоставляет http сервисы.

### Схема базы данных

![Схема БД](./doc/db_scheme.png)


### Сервер

Собран в docker-образ и запускается в связке с сервером postgersql через [docker-compose](docker-compose.yaml)

```
docker-compose up -d
```

Для тестирования grps-сервисов можно использовать [evans](https://github.com/ktr0731/evans)

```
evans --host 127.0.0.1 --port 9010 --path .\api\v1 --proto author.proto,history.proto,point.proto,reader.proto,service.proto
```

### Клиент

Собраны в исполняемые файлы в [artifact](https://gitlab.com/it-expertise/crossbooking/-/jobs/artifacts/master/download?job=Client) gitlab ci.

Или отдельно [клиент](https://gitlab.com/it-expertise/crossbooking/-/jobs/artifacts/master/raw/out/client/client.win64.exe?job=Client) Windows x64 

При запуске в параметрах доступно задать адрес grpc-сервера, адрес месторасположения точки, порт локального http-сервера

```
client.win64.exe -h
2021/02/11 07:39:55 Init client
Usage of client.win64.exe:
  -address string
        location address (default "Новосибирск 2")
  -port int
        listen http port (default 8081)
  -server string
        server (default "127.0.0.1:9009")
```

API веб-сервиса описано в [Swagger](swagger.yaml)