package main

import (
	"context"
	"fmt"
	"log"
	"net"
	"net/http"
	"os"
	"os/signal"
	"time"

	"crossbooking/pkg/api"
	handler "crossbooking/pkg/http"
	"crossbooking/pkg/point"

	"github.com/gorilla/mux"
	"google.golang.org/grpc"
)

var (
	ciCommitBranch,
	ciCommitTag,
	ciCommitShortSha,
	ciCommitSha,
	gitVersion string
)

func init() {
	fmt.Printf("Branch:%s\nTag:%s\nShortSha:%s\nSha:%s\nVersion:%s\n",
		ciCommitBranch,
		ciCommitTag,
		ciCommitShortSha,
		ciCommitSha,
		gitVersion,
	)
}

func main() {

	r := mux.NewRouter()
	r.HandleFunc("/", handler.HomeHandler)
	r.HandleFunc("/health", handler.HealthCheckHandler)
	r.HandleFunc("/author/{id:[0-9]+}", handler.GetAuthorByIDHandler).Methods("GET")

	http.Handle("/", r)

	srv := &http.Server{
		Addr: "0.0.0.0:8080",
		// Good practice to set timeouts to avoid Slowloris attacks.
		WriteTimeout: time.Second * 15,
		ReadTimeout:  time.Second * 15,
		IdleTimeout:  time.Second * 60,
		Handler:      r, // Pass our instance of gorilla/mux in.
	}

	go func() {
		log.Printf("http: starting %s\n", srv.Addr)
		if err := srv.ListenAndServe(); err != nil {
			log.Fatalf("failed to http serve: %s", err)
		}
	}()

	listen, err := net.Listen("tcp4", fmt.Sprintf("0.0.0.0:%d", 9009))
	if err != nil {
		log.Fatalf("failed to listen: %v", err)
	}

	grpcServer := grpc.NewServer()
	ps := &point.GRPCServer{}
	api.RegisterPointServiceServer(grpcServer, ps)

	go func() {
		log.Printf("grpc: starting %s\n", listen.Addr().String())
		if err := grpcServer.Serve(listen); err != nil {
			log.Fatalf("failed to grpc serve: %s", err)
		}
	}()

	c := make(chan os.Signal, 1)
	// We'll accept graceful shutdowns when quit via SIGINT (Ctrl+C)
	// SIGKILL, SIGQUIT or SIGTERM (Ctrl+/) will not be caught.
	signal.Notify(c, os.Interrupt)

	// Block until we receive our signal.
	<-c

	// Create a deadline to wait for.
	ctx, cancel := context.WithTimeout(context.Background(), time.Second*15)
	defer cancel()
	// Doesn't block if no connections, but will otherwise wait
	// until the timeout deadline.
	srv.Shutdown(ctx)
	grpcServer.Stop()
	// Optionally, you could run srv.Shutdown in a goroutine and block on
	// <-ctx.Done() if your application should wait for other services
	// to finalize based on context cancellation.
	log.Println("shutting down")
	os.Exit(0)
}
