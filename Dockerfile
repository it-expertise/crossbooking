FROM golang:latest as build

ARG BUILD_ID
ARG CI_COMMIT_BRANCH=unspecified
ARG CI_COMMIT_TAG=unspecified
ARG CI_COMMIT_SHORT_SHA=unspecified
ARG CI_COMMIT_SHA=unspecified
ARG GIT_VERSION=unspecified
ARG PROTOBUF_RELEASE=3.14.0

LABEL build=$BUILD_ID
LABEL stage=builder

ENV GO111MODULE=on \
    CI_COMMIT_BRANCH=$CI_COMMIT_BRANCH \
    CI_COMMIT_TAG=$CI_COMMIT_TAG \
    CI_COMMIT_SHORT_SHA=$CI_COMMIT_SHORT_SHA \
    CI_COMMIT_SHA=$CI_COMMIT_SHA \
    GIT_VERSION=$GIT_VERSION

RUN env

WORKDIR /app

COPY . .

RUN go mod download && \
    apt-get update && \
    apt-get install unzip && \
    curl -OL https://github.com/protocolbuffers/protobuf/releases/download/v$PROTOBUF_RELEASE/protoc-$PROTOBUF_RELEASE-linux-x86_64.zip && \
    unzip -o protoc-$PROTOBUF_RELEASE-linux-x86_64.zip -d /usr/local bin/protoc && \
    unzip -o protoc-$PROTOBUF_RELEASE-linux-x86_64.zip -d /usr/local include/*  && \
    go get github.com/golang/protobuf/protoc-gen-go && \
    protoc --proto_path=api/v1 --go_out=plugins=grpc:pkg api/v1/*.proto && \
    CGO_ENABLED=0 GOOS=linux go build -o ./out/server -ldflags "-X main.GitCommit=$GIT_COMMIT -X main.GitBranch=$GIT_BRANCH -X main.GitVersion=$GIT_VERSION" ./server/server.go
    # CGO_ENABLED=0 GOOS=linux GOARCH=amd64 go build -o ./out/client/client -ldflags "-X main.GitCommit=$GIT_COMMIT -X main.GitBranch=$GIT_BRANCH -X main.GitVersion=$GIT_VERSION" ./client/client.go && \
    # CGO_ENABLED=0 GOOS=windows GOARCH=amd64 go build -o ./out/client/client.win64.exe -ldflags "-X main.GitCommit=$GIT_COMMIT -X main.GitBranch=$GIT_BRANCH -X main.GitVersion=$GIT_VERSION" ./client/client.go && \
    # ls -lrt ./out/client

FROM alpine:latest as production
# Database setting
ENV	DB_HOST=timescaledb \
    DB_PORT=5432 \
    DB_NAME=postgres \
    DB_USER=postgres \
    DB_PASS=postgres

COPY --from=build /app/out/server server
CMD ["./server"]
EXPOSE 8080 9009