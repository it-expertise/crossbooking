DO $$
begin
	if not exists (
		select *
		from pg_tables
		where
			schemaname = 'public'
			and tablename = 'history')
	then
	
CREATE TABLE author (
	author_id int4 NOT NULL GENERATED ALWAYS AS IDENTITY,
	first_name varchar NULL,
	second_name varchar NULL,
	CONSTRAINT author_pk PRIMARY KEY (author_id)
);

CREATE TABLE reader (
	reader_id int4 NOT NULL GENERATED ALWAYS AS IDENTITY,
	first_name varchar NULL,
	second_name varchar NULL,
	email varchar NULL,
	phone varchar NULL,
	CONSTRAINT reader_pk PRIMARY KEY (reader_id)
);

CREATE TABLE point (
	point_id int4 NOT NULL GENERATED ALWAYS AS IDENTITY,
	address varchar NULL,
	CONSTRAINT point_pk PRIMARY KEY (point_id)
);

CREATE TABLE book (
	book_id int4 NOT NULL GENERATED ALWAYS AS IDENTITY,
	author_id int4 NULL REFERENCES author(author_id),
	title varchar NULL,
	CONSTRAINT book_pk PRIMARY KEY (book_id)
);
CREATE INDEX book_author_id_idx ON book USING btree (author_id);
	
CREATE TABLE history (
	history_id int4 NOT NULL GENERATED ALWAYS AS IDENTITY,
	dt timestamp(0) NULL DEFAULT now(),
	book_id int4 null REFERENCES book(book_id),
	state int2 NULL,
	point_id int4 null REFERENCES point(point_id),
	reader_id int4 null REFERENCES reader(reader_id),
	CONSTRAINT history_pk PRIMARY KEY (history_id)
);

CREATE INDEX history_book_id_idx ON history USING btree (book_id);
CREATE INDEX history_point_id_idx ON history USING btree (point_id);
CREATE INDEX history_reader_id_idx ON history USING btree (reader_id);

CREATE OR REPLACE VIEW history_last
AS SELECT DISTINCT ON (h.book_id) h.history_id,
    h.dt,
    h.book_id,
    h.point_id,
    h.reader_id,
    h.state
   FROM history h
  ORDER BY h.book_id, h.history_id DESC;
 
insert into	author (second_name)
values
	('Толкин'),
	('Чуковский'),
	('Лондон'),
	('Робертс'),
	('Пушкин');



with ins (title,second_name) as (
values
	( 'Властелин колец','Толкин') ,
	( 'Путешествие туда и обратно','Толкин' ),
	( 'Муха-Цокотуха','Чуковский' ),
	( 'Тараканище','Чуковский' ),
	( 'Зов предков','Лондон' ),
	( 'Белый клык','Лондон' ),
	( 'Шантарам','Робертс' ),
	( 'Тень Горы','Толкин' ),
	( 'Повести Белкина','Пушкин' ),
	( 'Медный всадник','Пушкин' )
)
insert into
	book (title , author_id)
select
	ins.title,
	author.author_id 
from
	author
join ins on
	ins.second_name = author.second_name ;



insert into	point (address)
values
	('Новосибирск 1'),
	('Новосибирск 2'),
	('Орел 1'),
	('Орел 2');



insert into	reader (first_name, second_name, email, phone)
values 
	('system', 'system', '', ''),
	('Леонтьев', 'Иосиф', 'LeontievIosif315@mail.ru', '8 (430) 150-18-18'),
	('Быков', 'Серафим', 'ByikovSerafim273@mail.ru', '8 (946) 205-81-24'),
	('Кузьмин', 'Елисей', 'KuziminElisey118@mail.ru', '8 (189) 944-77-53'),
	('Игнатьев', 'Герасим', 'IgnatievGerasim234@mail.ru', '8 (762) 701-23-59');



with ins (title,address,first_name,state) as (
values
	( 'Властелин колец','Новосибирск 1','system',0),
	( 'Путешествие туда и обратно','Новосибирск 1','system',0 ),
	( 'Муха-Цокотуха','Новосибирск 2','system',0 ),
	( 'Тараканище','Новосибирск 1','system',0 ),
	( 'Зов предков','Новосибирск 2','system',0 ),
	( 'Белый клык','Орел 2','system',0 ),
	( 'Шантарам','Орел 2','system',0 ),
	( 'Тень Горы','Орел 1','system',0 ),
	( 'Повести Белкина','Орел 2','system',0 ),
	( 'Медный всадник','Новосибирск 1','system',0 )
)
insert into
	history (book_id, point_id, reader_id, state)
select
	book.book_id,
	point.point_id,
	reader.reader_id,
	ins.state
from
	book
join ins on
	ins.title = book.title
join point on
	ins.address = point.address 
join reader on
	ins.first_name = reader.first_name;

	end if;
end $$;