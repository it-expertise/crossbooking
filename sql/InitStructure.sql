-- DROP TABLE public.author;

CREATE TABLE public.author (
	author_id int4 NOT NULL GENERATED ALWAYS AS IDENTITY,
	first_name varchar NULL,
	second_name varchar NULL,
	CONSTRAINT author_pk PRIMARY KEY (author_id)
);


-- DROP TABLE public.reader;

CREATE TABLE public.reader (
	reader_id int4 NOT NULL GENERATED ALWAYS AS IDENTITY,
	first_name varchar NULL,
	second_name varchar NULL,
	email varchar NULL,
	phone varchar NULL,
	CONSTRAINT reader_pk PRIMARY KEY (reader_id)
);

-- DROP TABLE public.point;

CREATE TABLE public.point (
	point_id int4 NOT NULL GENERATED ALWAYS AS IDENTITY,
	address varchar NULL,
	CONSTRAINT point_pk PRIMARY KEY (point_id)
);

-- DROP TABLE public.history;

CREATE TABLE public.history (
	history_id int4 NOT NULL GENERATED ALWAYS AS IDENTITY,
	dt timestamp(0) NULL DEFAULT now(),
	book_id int4 NULL,
	state int2 NULL,
	point_id int4 NULL,
	reader_id int4 NULL,
	CONSTRAINT history_pk PRIMARY KEY (history_id)
);

CREATE INDEX history_book_id_idx ON public.history USING btree (book_id);
CREATE INDEX history_point_id_idx ON public.history USING btree (point_id);
CREATE INDEX history_reader_id_idx ON public.history USING btree (reader_id);

ALTER TABLE public.history ADD CONSTRAINT history_fk FOREIGN KEY (book_id) REFERENCES book(book_id);
ALTER TABLE public.history ADD CONSTRAINT history_fk_1 FOREIGN KEY (point_id) REFERENCES point(point_id);
ALTER TABLE public.history ADD CONSTRAINT history_fk_2 FOREIGN KEY (reader_id) REFERENCES reader(reader_id);