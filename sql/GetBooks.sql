-- EXPLAIN (ANALYZE, COSTS, VERBOSE, BUFFERS, FORMAT JSON)

select
	b.book_id
	,b.title 
	,a.author_id 
	,a.first_name
	,a.second_name
	,hl.history_id 
	,hl.dt
	,hl.state 
	,p.point_id
	,p.address
	,r.reader_id
	,r.first_name
	,r.second_name
	,r.email
	,r.phone 
from history_last hl 
left join book b on b.book_id = hl.book_id 
left join author a on a.author_id = b.author_id
left join point p on p.point_id = hl.point_id 
left join reader r on r.reader_id = hl.reader_id 


