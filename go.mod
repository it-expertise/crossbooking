module crossbooking

go 1.15

require (
	github.com/caarlos0/env v3.5.0+incompatible
	github.com/golang/protobuf v1.4.2
	github.com/gorilla/mux v1.8.0
	github.com/jackc/pgx/v4 v4.10.1
	github.com/sirupsen/logrus v1.7.0 // indirect
	google.golang.org/grpc v1.35.0
	google.golang.org/protobuf v1.25.0
	gopkg.in/ffmt.v1 v1.5.6
)
