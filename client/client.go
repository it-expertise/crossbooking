package main

import (
	"context"
	"crossbooking/pkg/api"
	"encoding/json"
	"flag"
	"fmt"
	"log"
	"math/rand"
	"net/http"
	"os"
	"os/signal"
	"time"

	"github.com/golang/protobuf/jsonpb"
	"github.com/gorilla/mux"
	"google.golang.org/grpc"
)

var (
	server  string
	pointID int
	address string
	port    int

	addressList = []string{
		"",
		"Новосибирск 1",
		"Новосибирск 2",
		"Орел 1",
		"Орел 2",
	}

	client api.PointServiceClient
)

func init() {
	log.Println("Init client")

	rand.Seed(time.Now().UnixNano())

	randomID := rand.Intn(4) + 1

	flag.StringVar(&server, "server", "127.0.0.1:9009", "server")
	flag.StringVar(&address, "address", addressList[randomID], "location address")
	flag.IntVar(&port, "port", 8081, "listen http port")
	flag.Parse()
	log.Printf("Config:\n\tserver: %s\n\taddress: %s\n\tport: %d\n", server, address, port)
}

func main() {
	log.Println("Starting client")

	conn, err := grpc.Dial(server, grpc.WithInsecure())

	if err != nil {
		log.Fatalf("not connected %s: %s\n", server, err.Error())
	}

	client = api.NewPointServiceClient(conn)
	resp, err := client.Connect(context.Background(), &api.ConnectRequest{Address: address})
	if err != nil {
		log.Fatalf("exec Connect error %s\n", err.Error())
	}

	if resp.Result == api.ResultType_SUCCESS {
		pointID = int(resp.GetId())
	}

	log.Printf("pointID=%d\n", pointID)

	r := mux.NewRouter()
	r.HandleFunc("/", HomeHandler).Methods("GET")
	r.HandleFunc("/books", GetBooksHandler).Methods("GET")
	r.HandleFunc("/book", SetBookStateHandler).Methods("PUT")
	r.HandleFunc("/readers", GetReadersHandler).Methods("GET")
	r.HandleFunc("/authors", GetAuthorsHandler).Methods("GET")

	http.Handle("/", r)

	srv := &http.Server{
		Addr: fmt.Sprintf("0.0.0.0:%d", port),
		// Good practice to set timeouts to avoid Slowloris attacks.
		WriteTimeout: time.Second * 15,
		ReadTimeout:  time.Second * 15,
		IdleTimeout:  time.Second * 60,
		Handler:      r, // Pass our instance of gorilla/mux in.
	}

	go func() {
		log.Printf("http: starting %s\n", srv.Addr)
		if err := srv.ListenAndServe(); err != nil {
			log.Fatalf("failed to http serve: %s", err)
		}
	}()

	c := make(chan os.Signal, 1)
	// We'll accept graceful shutdowns when quit via SIGINT (Ctrl+C)
	// SIGKILL, SIGQUIT or SIGTERM (Ctrl+/) will not be caught.
	signal.Notify(c, os.Interrupt)

	// Block until we receive our signal.
	<-c

	// Create a deadline to wait for.
	ctx, cancel := context.WithTimeout(context.Background(), time.Second*15)
	defer cancel()
	// Doesn't block if no connections, but will otherwise wait
	// until the timeout deadline.
	srv.Shutdown(ctx)
	// Optionally, you could run srv.Shutdown in a goroutine and block on
	// <-ctx.Done() if your application should wait for other services
	// to finalize based on context cancellation.
	log.Println("shutting down")
	os.Exit(0)

}

// HomeHandler .
func HomeHandler(w http.ResponseWriter, r *http.Request) {
	w.WriteHeader(http.StatusOK)
	w.Write([]byte("Home"))
}

//GetBooksHandler .
func GetBooksHandler(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content-Type", "application/json")
	w.Header().Set("Cache-Control", "private, max-age=0, no-cache")

	resp, err := client.GetBooks(context.Background(), &api.GetBooksRequest{})
	if err == nil {
		w.WriteHeader(http.StatusOK)
		m := jsonpb.Marshaler{
			EmitDefaults: true,
		}
		js, _ := m.MarshalToString(resp)
		w.Write([]byte(js))
	} else {
		w.WriteHeader(http.StatusInternalServerError)
		json.NewEncoder(w).Encode(err.Error())
	}
}

//SetBookStateHandler .
func SetBookStateHandler(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content-Type", "application/json")
	w.Header().Set("Cache-Control", "private, max-age=0, no-cache")

	req := api.SetBookStateRequest{
		PointId: uint32(pointID),
	}

	err := jsonpb.Unmarshal(r.Body, &req)
	if err != nil {
		w.WriteHeader(http.StatusBadRequest)
		return
	}

	resp, err := client.SetBookState(context.Background(), &req)
	if err == nil {
		w.WriteHeader(http.StatusOK)
		m := jsonpb.Marshaler{
			EmitDefaults: true,
		}
		js, _ := m.MarshalToString(resp)
		w.Write([]byte(js))
	} else {
		w.WriteHeader(http.StatusInternalServerError)
		json.NewEncoder(w).Encode(err.Error())
	}
}

//GetReadersHandler .
func GetReadersHandler(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content-Type", "application/json")
	w.Header().Set("Cache-Control", "private, max-age=0, no-cache")

	resp, err := client.GetReaders(context.Background(), &api.GetReadersRequest{})
	if err == nil {
		w.WriteHeader(http.StatusOK)
		m := jsonpb.Marshaler{
			EmitDefaults: true,
		}
		js, _ := m.MarshalToString(resp)
		w.Write([]byte(js))
	} else {
		w.WriteHeader(http.StatusInternalServerError)
		json.NewEncoder(w).Encode(err.Error())
	}
}

//GetAuthorsHandler .
func GetAuthorsHandler(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content-Type", "application/json")
	w.Header().Set("Cache-Control", "private, max-age=0, no-cache")

	resp, err := client.GetAuthors(context.Background(), &api.GetAuthorsRequest{})
	if err == nil {
		w.WriteHeader(http.StatusOK)
		m := jsonpb.Marshaler{
			EmitDefaults: true,
		}
		js, _ := m.MarshalToString(resp)
		w.Write([]byte(js))
	} else {
		w.WriteHeader(http.StatusInternalServerError)
		json.NewEncoder(w).Encode(err.Error())
	}
}
