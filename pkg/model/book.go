package models

import (
	"context"
	"log"

	"github.com/jackc/pgx/v4"
)

// Book .
type Book struct {
	BookID int
	Title  *string

	Author  Author
	History History
	Point   Point
	Reader  Reader
}

// GetBooks .
func GetBooks() ([]*Book, error) {
	books := make([]*Book, 0)

	c, err := ConnectDBRO()
	if err == nil {
		var rows pgx.Rows
		rows, err = c.Query(context.Background(), `
select
	b.book_id
	,b.title 
	,a.author_id 
	,a.first_name
	,a.second_name
	,hl.history_id 
	,hl.dt
	,hl.state 
	,p.point_id
	,p.address
	,r.reader_id
	,r.first_name
	,r.second_name
	,r.email
	,r.phone 
from history_last hl 
left join book b on b.book_id = hl.book_id 
left join author a on a.author_id = b.author_id
left join point p on p.point_id = hl.point_id
left join reader r on r.reader_id = hl.reader_id;
`)
		if err != nil && err != pgx.ErrNoRows {
			log.Printf("QueryRows failed: %v\n", err)
		} else {
			for rows.Next() {
				book := Book{}
				err = rows.Scan(
					&book.BookID,
					&book.Title,
					&book.Author.AuthorID,
					&book.Author.FirstName,
					&book.Author.SecondName,
					&book.History.HistoryID,
					&book.History.DT,
					&book.History.State,
					&book.Point.PointID,
					&book.Point.Address,
					&book.Reader.ReaderID,
					&book.Reader.FirstName,
					&book.Reader.SecondName,
					&book.Reader.Email,
					&book.Reader.Phone,
				)
				if err != nil {
					log.Printf("scan failed: %v\n", err)
				}
				books = append(books, &book)
			}
		}

	}
	c.Release()

	// ffmt.P(books)

	return books, err
}

// SetBookState .
func SetBookState(pointID int, bookID int, readerID int, state int) error {
	c, err := ConnectDBRW()
	if err == nil {
		_, err = c.Exec(context.Background(), `INSERT INTO history (point_id, book_id, reader_id, state) VALUES($1, $2, $3, $4)`,
			pointID,
			bookID,
			readerID,
			state,
		)
	}
	c.Release()

	return err
}
