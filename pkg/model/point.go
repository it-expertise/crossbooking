package models

import (
	"context"
	"log"

	"github.com/jackc/pgx/v4"
)

// Point .
type Point struct {
	PointID int
	Address *string
}

// GePointByID .
func GePointByID(id int) (Point, error) {
	point := Point{
		PointID: id,
	}

	c, err := ConnectDBRO()
	if err == nil {
		err = c.QueryRow(context.Background(), "select address from point where point_id=$1", id).Scan(&point.Address)
		if err != nil && err != pgx.ErrNoRows {
			log.Printf("QueryRow failed: %v\n", err)
		}
	}
	c.Release()

	return point, err
}

// GePointByAddress .
func GePointByAddress(adr string) (Point, error) {
	point := Point{
		Address: &adr,
	}

	c, err := ConnectDBRO()
	if err == nil {
		err = c.QueryRow(context.Background(), "select point_id from point where address=$1", adr).Scan(&point.PointID)
		if err != nil && err != pgx.ErrNoRows {
			log.Printf("QueryRow failed: %v\n", err)
		}
	}
	c.Release()

	return point, err
}

// AddPoint .
func AddPoint(adr string) (int, error) {
	var id int

	c, err := ConnectDBRW()

	if err == nil {
		err = c.QueryRow(context.Background(), "INSERT INTO point (address) VALUES ($1) returning (point_id)", adr).Scan(&id)
		if err != nil && err != pgx.ErrNoRows {
			log.Printf("QueryRow failed: %v\n", err)
		}
	}
	c.Release()

	return id, err
}
