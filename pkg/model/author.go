package models

import (
	"context"
	"log"

	"github.com/jackc/pgx/v4"
)

// Author .
type Author struct {
	AuthorID   int
	FirstName  *string
	SecondName *string
}

// GetAuthorByID .
func GetAuthorByID(id int) (Author, error) {
	author := Author{}

	c, err := ConnectDBRO()
	if err == nil {
		err = c.QueryRow(context.Background(), "select author_id, first_name, second_name from author where author_id=$1", id).Scan(&author.AuthorID, &author.FirstName, &author.SecondName)
		if err != nil && err != pgx.ErrNoRows {
			log.Printf("QueryRow failed: %v\n", err)
		}
	}
	c.Release()

	return author, err
}

// GetAuthors *
func GetAuthors() ([]*Author, error) {
	authors := make([]*Author, 0)

	c, err := ConnectDBRO()
	if err == nil {
		var rows pgx.Rows
		rows, err = c.Query(context.Background(), `select author_id, first_name, second_name from author;`)
		if err != nil && err != pgx.ErrNoRows {
			log.Printf("QueryRows failed: %v\n", err)
		} else {
			for rows.Next() {
				author := Author{}
				err = rows.Scan(
					&author.AuthorID,
					&author.FirstName,
					&author.SecondName,
				)
				if err != nil {
					log.Printf("scan failed: %v\n", err)
				}
				authors = append(authors, &author)
			}
		}

	}
	c.Release()

	return authors, err
}
