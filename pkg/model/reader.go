package models

import (
	"context"
	"log"

	"github.com/jackc/pgx/v4"
)

// Reader .
type Reader struct {
	ReaderID   int
	FirstName  *string
	SecondName *string
	Email      *string
	Phone      *string
}

// GetReaders *
func GetReaders() ([]*Reader, error) {
	readers := make([]*Reader, 0)

	c, err := ConnectDBRO()
	if err == nil {
		var rows pgx.Rows
		rows, err = c.Query(context.Background(), `select reader_id, first_name, second_name, email, phone from reader;`)
		if err != nil && err != pgx.ErrNoRows {
			log.Printf("QueryRows failed: %v\n", err)
		} else {
			for rows.Next() {
				reader := Reader{}
				err = rows.Scan(
					&reader.ReaderID,
					&reader.FirstName,
					&reader.SecondName,
					&reader.Email,
					&reader.Phone,
				)
				if err != nil {
					log.Printf("scan failed: %v\n", err)
				}
				readers = append(readers, &reader)
			}
		}

	}
	c.Release()

	return readers, err
}
