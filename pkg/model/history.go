package models

import (
	"time"
)

// History .
type History struct {
	HistoryID int
	DT        time.Time
	BookID    int
	State     int
	PointID   int
	ReaderID  int
}
