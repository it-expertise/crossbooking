package point

import (
	"context"

	"crossbooking/pkg/api"
	models "crossbooking/pkg/model"

	"github.com/golang/protobuf/ptypes"
	"github.com/jackc/pgx/v4"
)

// GRPCServer .
type GRPCServer struct{}

// Connect .
func (s *GRPCServer) Connect(ctx context.Context, req *api.ConnectRequest) (*api.ConnectResponse, error) {
	resp := &api.ConnectResponse{
		Result: api.ResultType_SUCCESS,
	}

	p, err := models.GePointByAddress(req.Address)
	if err == nil {
		resp.Id = uint32(p.PointID)
	} else {
		if err == pgx.ErrNoRows {
			var id int
			if id, err = models.AddPoint(req.Address); err == nil {
				resp.Id = uint32(id)
			} else {
				resp.Result = api.ResultType_FAIL
			}
		}
	}

	return resp, err
}

// GetBooks .
func (s *GRPCServer) GetBooks(ctx context.Context, req *api.GetBooksRequest) (*api.GetBooksResponse, error) {
	resp := &api.GetBooksResponse{
		Books: make([]*api.Book, 0),
	}
	books, err := models.GetBooks()
	if err == nil {
		for _, v := range books {
			b := &api.Book{
				BookId: uint32(v.BookID),
				Title:  *v.Title,
				Author: &api.Author{
					AuthorId:   uint32(v.Author.AuthorID),
					SecondName: *v.Author.SecondName,
				},
				History: &api.History{
					HistoryId: uint32(v.History.HistoryID),
					State:     uint32(v.History.State),
				},
				Point: &api.Point{
					PointId: uint32(v.Point.PointID),
					Address: *v.Point.Address,
				},
				Reader: &api.Reader{
					ReaderId: uint32(v.Reader.ReaderID),
				},
			}
			if v.Author.FirstName != nil {
				(*(*b).Author).FirstName = *v.Author.FirstName
			}
			b.History.Dt, _ = ptypes.TimestampProto(v.History.DT)

			if v.Reader.FirstName != nil {
				(*(*b).Reader).FirstName = *v.Reader.FirstName
			}

			if v.Reader.SecondName != nil {
				(*(*b).Reader).SecondName = *v.Reader.SecondName
			}

			if v.Reader.Email != nil {
				(*(*b).Reader).Email = *v.Reader.Email
			}

			if v.Reader.Phone != nil {
				(*(*b).Reader).Phone = *v.Reader.Phone
			}

			resp.Books = append(resp.Books, b)
		}
	}

	return resp, nil
}

// SetBookState .
func (s *GRPCServer) SetBookState(ctx context.Context, req *api.SetBookStateRequest) (*api.SetBookStateResponse, error) {
	var err error
	resp := &api.SetBookStateResponse{}

	if err = models.SetBookState(
		int(req.PointId),
		int(req.BookId),
		int(req.ReaderId),
		int(req.State),
	); err == nil {
		resp.Result = api.ResultType_SUCCESS
	}

	return resp, err
}

// GetReaders .
func (s *GRPCServer) GetReaders(ctx context.Context, req *api.GetReadersRequest) (*api.GetReadersResponse, error) {
	var err error
	resp := &api.GetReadersResponse{
		Readers: make([]*api.Reader, 0),
	}

	readers, err := models.GetReaders()
	if err == nil {
		for _, v := range readers {
			r := &api.Reader{
				ReaderId: uint32(v.ReaderID),
			}

			if v.FirstName != nil {
				(*r).FirstName = *v.FirstName
			}

			if v.SecondName != nil {
				(*r).SecondName = *v.SecondName
			}

			if v.Email != nil {
				(*r).Email = *v.Email
			}

			if v.Phone != nil {
				(*r).Phone = *v.Phone
			}

			resp.Readers = append(resp.Readers, r)
		}
	}

	return resp, err
}

// GetAuthors .
func (s *GRPCServer) GetAuthors(ctx context.Context, req *api.GetAuthorsRequest) (*api.GetAuthorsResponse, error) {
	var err error
	resp := &api.GetAuthorsResponse{
		Authors: make([]*api.Author, 0),
	}

	authors, err := models.GetAuthors()
	if err == nil {
		for _, v := range authors {
			a := &api.Author{
				AuthorId: uint32(v.AuthorID),
			}

			if v.FirstName != nil {
				(*a).FirstName = *v.FirstName
			}

			if v.SecondName != nil {
				(*a).SecondName = *v.SecondName
			}

			resp.Authors = append(resp.Authors, a)
		}
	}

	return resp, err
}
